<?php
require_once __DIR__ . '/DAO.php';

class DataDAO extends DAO {

  public function get_store($id) {
    $sql = "SELECT * FROM `stores` WHERE `id` = :id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $id);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function insert_order($data) {
    $sql = "INSERT INTO `store_orders` (`store_id`,`store_view_id`,`order_id`,`base_total_paid`,`date`) VALUES (:store_id,:store_view_id,:order_id,:base_total_paid,:date)";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':store_id', $data['store_id']);
    $stmt->bindValue(':store_view_id', $data['store_view_id']);
    $stmt->bindValue(':order_id', $data['order_id']);
    $stmt->bindValue(':base_total_paid', $data['base_total_paid']);
    $stmt->bindValue(':date', $data['date']);
    $stmt->execute();
  }

  public function get_stores_not_synced_magento1() {
    $sql = "SELECT * FROM `stores` WHERE `synced_firsttime` = 0 AND `store_type` = 'magento1'";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function update_store_sync($id) {
    $sql = "UPDATE `stores` SET `synced_firsttime` = 1 WHERE `id` = :id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $id);
    $stmt->execute();
  }

  public function check_order($data) {
    $sql = "SELECT `id` FROM `store_orders`
            WHERE `store_id` = :store_id
            AND `order_id` = :order_id
            AND `date` = :date";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':store_id', $data['store_id']);
    $stmt->bindValue(':order_id', $data['order_id']);
    $stmt->bindValue(':date', $data['date']);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

}
