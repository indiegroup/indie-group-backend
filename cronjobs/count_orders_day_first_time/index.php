<?php

date_default_timezone_set('Europe/Amsterdam');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
print_r('<pre>');

$error_log = '/home/admin/domains/indiegroup.be/public_html/cronjobs/count_orders_day_first_time/errors.log';
// $error_log = 'errors.log';

try{
  require __DIR__ . '/dao/DataDAO.php';
  $dataDAO = new DataDAO();

  $stores = $dataDAO->get_stores();

  foreach($stores as $store){
    $firstOrder = $dataDAO->get_first_order($store['id']);

    if(empty($firstOrder)){
      break;
    }

    $date_from = date('Y-m-d 00:00:00', strtotime($firstOrder['date']));
    $date_to = date('Y-m-d');

    $diff = strtotime($date_to) - strtotime($date_from);
    $days = round($diff / (60 * 60 * 24));

    for ($i=0; $i < $days; $i++) {
      $new_date_from = date('Y-m-d 00:00:00', strtotime('+'.$i.' day', strtotime($date_from)));
      $new_date_to = date('Y-m-d 23:59:59', strtotime('+'.$i .' day', strtotime($date_from)));

      $check = $dataDAO->check_order(array(
        'date_from' => $new_date_from,
        'store_id' => $store['id']
      ));

      if(empty($check)){
        $orders = $dataDAO->get_orders(array(
          'date_from' => $new_date_from,
          'date_to' => $new_date_to,
          'store_id' => $store['id']
        ));

        $total_price = 0;

        if(!empty($orders)){
          foreach($orders as $order){
            $total_price += (int)$order['base_total_paid'];
          }
          $result = array(
            'average_order' => round($total_price / count($orders), 2),
            'total_revenue' => $total_price,
            'amount_orders' => count($orders)
          );
        }else{
          $result = array(
            'average_order' => 0,
            'total_revenue' => 0,
            'amount_orders' => 0
          );
        }

        $dataDAO->insert_order(array(
          'store_id' => $store['id'],
          'average_order' => $result['average_order'],
          'total_revenue' => $result['total_revenue'],
          'amount_orders' => $result['amount_orders'],
          'date' => $new_date_from
        ));

      }

    }

    $dataDAO->update_store_sync($store['id']);

  }
}catch(Exception $e){
  error_log(date('Y-m-d H:i:s').' : '.$e->getMessage()."\n", 3, $error_log);
}
