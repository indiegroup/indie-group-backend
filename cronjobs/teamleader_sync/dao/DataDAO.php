<?php
require_once __DIR__ . '/DAO.php';

class DataDAO extends DAO {


  public function get_stores_not_synced() {
    $sql = "SELECT * FROM `stores` WHERE `synced_firsttime` = 0";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function get_token() {
    $sql = "SELECT * FROM `teamleader_token`";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function update_token($token) {
    $sql = "UPDATE `teamleader_token` SET `token` = :token WHERE `id` = 1";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':token', $token);
    $stmt->execute();
  }





}
