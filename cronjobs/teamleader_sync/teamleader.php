<?php

require __DIR__ . '/dao/DataDAO.php';

function get_teamleader_client_by_name($name){
    $dataDAO = new DataDAO();

    $token = $dataDAO->get_token();
    $accessToken = refresh_token($token);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.teamleader.eu/companies.list");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $filter = array(
      'filter'=> array('term' => $name),
      'page'=> array('size' => 10, 'number' => 1),
      'sort' => array(array('field' => 'added_at','order' => 'asc'))
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($filter));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
      "Authorization: Bearer " . $accessToken['access_token']
    ));

    $response = curl_exec($ch);
    $data = json_decode($response, true);
    curl_close($ch);

    return $data;
}

function get_teamleader_client_info($id){
    $dataDAO = new DataDAO();

    $token = $dataDAO->get_token();
    $accessToken = refresh_token($token);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.teamleader.eu/companies.info");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $filter = array(
      'id'=> $id
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($filter));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
      "Authorization: Bearer " . $accessToken['access_token']
    ));

    $response = curl_exec($ch);
    $data = json_decode($response, true);
    curl_close($ch);

    return $data;
}

function refresh_token($token){
  $dataDAO = new DataDAO();

  $clientId = '00bbc20577c2ac19993b60763435685a';
  $clientSecret = 'e7f2f88b868fa450ef45d048df291b06';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://app.teamleader.eu/oauth2/access_token');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, [
      'client_id' => $clientId,
      'client_secret' => $clientSecret,
      'refresh_token' => $token['token'],
      'grant_type' => 'refresh_token',
  ]);

  $response = curl_exec($ch);
  $data = json_decode($response, true);
  $accessToken = $data['access_token'];
  if(isset($accessToken) && !empty($accessToken)){
    $dataDAO->update_token($data['refresh_token']);
  }
  return $data;
}


 ?>
