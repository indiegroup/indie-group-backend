<?php

date_default_timezone_set('Europe/Amsterdam');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
print_r('<pre>');

// $error_log = '/home/admin/domains/indiegroup.be/public_html/cronjobs/sync_20_first_mins/errors.log';
$error_log = 'errors.log';

error_log("\n\nStart script datum:".date('Y-m-d H:i:s')."\n", 3, $error_log);

try{
  require __DIR__ . '/magento1.php';
  require __DIR__ . '/magento2.php';
  require __DIR__ . '/dao/DataDAO.php';
  require __DIR__ . '/encryption.php';

  $dataDAO = new DataDAO();
} catch (Exception $e) {
    error_log("\n\n".$e->getMessage(), 3, $error_log);
}

$stores = $dataDAO->get_stores();

foreach($stores as $store){
  error_log("ophalen store: ".$store['store_name']." datum:".date('Y-m-d H:i:s')."\n", 3, $error_log);

  $date_from = date('Y-m-d 00:00:00', strtotime('-7 day', strtotime(date('Y-m-d'))));
  $date_to = date('Y-m-d');

  $diff = strtotime($date_to) - strtotime($date_from);
  $days = round($diff / (60 * 60 * 24));

  for ($i=0; $i < $days; $i++) {
    sleep(1);
    error_log("\nophalen dag:".$i." storename: ".$store['store_name']." datum:".date('Y-m-d H:i:s'), 3, $error_log);

    $new_date_from = date('Y-m-d', strtotime('+'.$i.' day', strtotime($date_from)));
    $new_date_to = date('Y-m-d', strtotime('+'.$i + 1 .' day', strtotime($date_from)));

    try{
      if($store['store_type'] == 'magento1'){
        $orders = magento1($store, $new_date_from, $new_date_to);
      }
      if($store['store_type'] == 'magento2'){
        $orders = magento2($store, $new_date_from, $new_date_to);
      }

      if(!empty($orders)){
        foreach($orders as $order){
          if($store['store_type'] == 'magento1'){
            $order_id = $order['order_id'];
          }else{
            $order_id = $order['entity_id'];
          }
          $check = $dataDAO->check_order(array(
            'store_id' => $store['id'],
            'order_id' => $order_id,
            'date' => $order['created_at']
          ));
          if(empty($check)){
            $dataDAO->insert_order(array(
              'store_id' => $store['id'],
              'store_view_id' => $order['store_id'],
              'order_id' => $order_id,
              'base_total_paid' => round($order['base_total_paid'], 2),
              'date' => $order['created_at']
            ));
          }
        }
      }
    } catch (Exception $e) {
        error_log("\n\n".$e->getMessage(), 3, $error_log);
        break;
    }

    $check_count = $dataDAO->check_order_day(array(
      'date_from' => $new_date_from,
      'store_id' => $store['id']
    ));

    if(empty($check_count)){
      try{
        $orders = $dataDAO->get_orders(array(
          'date_from' => $new_date_from,
          'date_to' => $new_date_to,
          'store_id' => $store['id']
        ));

        $total_price = 0;

        if(!empty($orders)){
          foreach($orders as $order){
            $total_price += (int)$order['base_total_paid'];
          }
          $result = array(
            'average_order' => round($total_price / count($orders), 2),
            'total_revenue' => $total_price,
            'amount_orders' => count($orders)
          );
        }else{
          $result = array(
            'average_order' => 0,
            'total_revenue' => 0,
            'amount_orders' => 0
          );
        }

        $dataDAO->insert_order_count(array(
          'store_id' => $store['id'],
          'average_order' => $result['average_order'],
          'total_revenue' => $result['total_revenue'],
          'amount_orders' => $result['amount_orders'],
          'date' => $new_date_from
        ));
      } catch (Exception $e) {
          error_log("\n\n".$e->getMessage(), 3, $error_log);
         break;
      }
    }

  }
  error_log("\nophalen store EINDE: ".$store['store_name']." datum:".date('Y-m-d H:i:s')."----------------------------------------------\n", 3, $error_log);

  $dataDAO->change_status($store['id']);
}

error_log("\nEinde script datum:".date('Y-m-d H:i:s')."-------------------------------------------------------------\n\n", 3, $error_log);
