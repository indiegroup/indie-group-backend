<?php

function magento1($store, $new_date_from, $new_date_to){
  $apisoap_v2_url = $store['url']."/api/v2_soap/?wsdl";
  $password = encrypt_decrypt('decrypt',$store['password']);
  $client = new SoapClient($apisoap_v2_url);
  $session_id = $client->login($store['username'], $password);

  $filter = array(
    'filter' => array(array('key' => 'status','value' => 'complete')),
    'complex_filter' => array(
      array(
            'key' => 'CREATED_AT',
            'value' => array(
                'key' => 'from',
                'value' => $new_date_from
            ),
        ),
        array(
            'key' => 'created_at',
            'value' => array(
                'key' => 'to',
                'value' => $new_date_to
            ),
        ),
  ));

  $orders = $client->salesOrderList($session_id, $filter);
  $orders = json_decode(json_encode($orders), true);

  $filter2 = array(
    'filter' => array(array('key' => 'status','value' => 'processing')),
    'complex_filter' => array(
      array(
            'key' => 'CREATED_AT',
            'value' => array(
                'key' => 'from',
                'value' => $new_date_from
            ),
        ),
        array(
            'key' => 'created_at',
            'value' => array(
                'key' => 'to',
                'value' => $new_date_to
            ),
        ),
  ));

  $orders2 = $client->salesOrderList($session_id, $filter2);
  $orders2 = json_decode(json_encode($orders2), true);

  return array_merge($orders, $orders2);
}
