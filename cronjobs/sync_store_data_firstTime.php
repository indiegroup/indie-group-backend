<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
print_r('<pre>');

require __DIR__ . '/encryption.php';
require __DIR__ . '/dao/DataDAO.php';
$dataDAO = new DataDAO();

$stores = $dataDAO->get_stores_not_synced_magento1();

foreach($stores as $store){
  $date_from = '2020-04-20';
  $date_to = date('Y-m-d');

  $diff = strtotime($date_to) - strtotime($date_from);
  $days = round($diff / (60 * 60 * 24));

  for ($i=0; $i < $days; $i++) {
    sleep(1);

    $new_date_from = date('Y-m-d', strtotime('+'.$i.' day', strtotime($date_from)));
    $new_date_to = date('Y-m-d', strtotime('+'.$i + 1 .' day', strtotime($date_from)));

    $apisoap_v2_url = $store['url']."/api/v2_soap/?wsdl";
    $password = encrypt_decrypt('decrypt',$store['password']);
    $client = new SoapClient($apisoap_v2_url);
    $session_id = $client->login($store['username'], $password);
    $result = $client->magentoInfo($session_id);

    $filter = array(
      'filter' => array(array('key' => 'status','value' => 'complete')),
      'complex_filter' => array(
        array(
              'key' => 'UPDATED_AT',
              'value' => array(
                  'key' => 'from',
                  'value' => $new_date_from
              ),
          ),
          array(
              'key' => 'updated_at',
              'value' => array(
                  'key' => 'to',
                  'value' => $new_date_to
              ),
          ),
    ));

    $orders = $client->salesOrderList($session_id, $filter);
    $orders = json_decode(json_encode($orders), true);
    print_r($orders);
    foreach($orders as $order){
      $check = $dataDAO->check_order(array(
        'store_id' => $store['id'],
        'order_id' => $order['increment_id'],
        'date' => $order['updated_at']
      ));
      if(empty($check)){
        $dataDAO->insert_order(array(
          'store_id' => $store['id'],
          'store_view_id' => $order['store_id'],
          'order_id' => $order['increment_id'],
          'base_total_paid' => round($order['base_total_paid'], 2),
          'date' => $order['updated_at']
        ));
      }
    }
  }

  // $dataDAO->update_store_sync($store['id']);
  print_r('done');
}
