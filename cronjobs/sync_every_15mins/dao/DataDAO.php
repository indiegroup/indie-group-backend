<?php
require_once __DIR__ . '/DAO.php';

class DataDAO extends DAO {

  public function insert_order($data) {
    $sql = "INSERT INTO `store_orders` (`store_id`,`store_view_id`,`order_id`,`base_total_paid`,`date`) VALUES (:store_id,:store_view_id,:order_id,:base_total_paid,:date)";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':store_id', $data['store_id']);
    $stmt->bindValue(':store_view_id', $data['store_view_id']);
    $stmt->bindValue(':order_id', $data['order_id']);
    $stmt->bindValue(':base_total_paid', $data['base_total_paid']);
    $stmt->bindValue(':date', $data['date']);
    $stmt->execute();
  }

  public function check_order($data) {
    $sql = "SELECT `id` FROM `store_orders`
            WHERE `store_id` = :store_id
            AND `order_id` = :order_id
            AND `date` = :date";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':store_id', $data['store_id']);
    $stmt->bindValue(':order_id', $data['order_id']);
    $stmt->bindValue(':date', $data['date']);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function get_stores() {
    $sql = "SELECT * FROM `stores`";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function check_sync($data) {
    $sql = "SELECT * FROM `latest_sync` WHERE `store_id` = :store_id AND `latest_sync` = :date_to";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':store_id', $data['store_id']);
    $stmt->bindValue(':date_to', $data['date_to']);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function add_latest_sync($data) {
    $sql = "INSERT INTO `latest_sync` (`store_id`,`latest_sync`) VALUES (:store_id,:latest_sync)";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':store_id', $data['store_id']);
    $stmt->bindValue(':latest_sync', $data['date_to']);
    $stmt->execute();
  }







}
