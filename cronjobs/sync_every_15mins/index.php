<?php

date_default_timezone_set('Europe/Amsterdam');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
print_r('<pre>');

$GLOBALS['log'] = '/home/admin/domains/indiegroup.be/public_html/cronjobs/sync_every_15mins/errors.log';
// $GLOBALS['log'] = 'errors.log';

error_log("Start sync 15min datum: ".date('Y-m-d H:i:s')."\n\n", 3, $GLOBALS['log']);

require __DIR__ . '/magento1.php';
require __DIR__ . '/magento2.php';
require __DIR__ . '/dao/DataDAO.php';
require __DIR__ . '/encryption.php';

$dataDAO = new DataDAO();


$date_from = date('Y-m-d 23:45:00', strtotime('-1 day', strtotime(date('Y-m-d'))));
$date_to = date('Y-m-d 23:59:59', strtotime(date('Y-m-d')));

error_log("date From: ".$date_from."\n", 3, $GLOBALS['log']);
error_log("date To: ".$date_to."\n", 3, $GLOBALS['log']);


$current_min = date('i');
$minutes = 0;

if($current_min >= 0 && $current_min < 15){
  $minutes = 45;
}

if($current_min >= 15 && $current_min < 30){
  $minutes = 0;
}

if($current_min >= 30 && $current_min < 45){
  $minutes = 15;
}

if($current_min >= 45 && $current_min < 60){
  $minutes = 30;
}

if($minutes == 45){
  $date_sync = date('Y-m-d H:i:00', strtotime('-1 hour +15 min', strtotime(date('Y-m-d H:'.$minutes))));
}else{
  $date_sync = date('Y-m-d H:i:00', strtotime('+15 min', strtotime(date('Y-m-d H:'.$minutes))));
}

$stores = $dataDAO->get_stores();


foreach($stores as $store){
  $GLOBALS['error'] = 0;
  error_log("\nStore: ".$store['store_name']."\n", 3, $GLOBALS['log']);

  sync($store,$date_from,$date_to);
  if($GLOBALS['error'] == 0){
    $dataDAO->add_latest_sync(array(
      'store_id' => $store['id'],
      'date_to' => $date_sync
    ));
  }

}

function sync($store,$date_from,$date_to){
  error_log("Run type: ".$store['store_type']."\n", 3, $GLOBALS['log']);
  $dataDAO = new DataDAO();

  if($store['store_type'] == 'magento1'){
    try{
      $orders = magento1($store, $date_from, $date_to);
    }catch(Exception $e){
      $GLOBALS['error'] = 1;
      error_log(date('Y-m-d H:i:s').' : '.$e->getMessage()."\n", 3, $GLOBALS['log']);
    }
  }
  if($store['store_type'] == 'magento2'){
    try{
      $orders = magento2($store, $date_from, $date_to);
    }catch(Exception $e){
      $GLOBALS['error'] = 1;
      error_log(date('Y-m-d H:i:s').' : '.$e->getMessage()."\n", 3, $GLOBALS['log']);
    }
  }

  print_r($orders);

  if(!empty($orders)){
    foreach($orders as $order){

      if($store['store_type'] == 'magento1'){
        $order_id = $order['order_id'];
      }else{
        $order_id = $order['entity_id'];
      }

      error_log('orderId: '.$order_id."\n", 3, $GLOBALS['log']);

      $check = $dataDAO->check_order(array(
        'store_id' => $store['id'],
        'order_id' => $order_id,
        'date' => $order['created_at']
      ));
      if(empty($check)){
        $dataDAO->insert_order(array(
          'store_id' => $store['id'],
          'store_view_id' => $order['store_id'],
          'order_id' => $order_id,
          'base_total_paid' => round($order['base_total_paid'], 2),
          'date' => $order['created_at']
        ));
      }
    }
  }

}

error_log("\nEND sync 15min datum:".date('Y-m-d H:i:s')."\n\n", 3, $GLOBALS['log']);
