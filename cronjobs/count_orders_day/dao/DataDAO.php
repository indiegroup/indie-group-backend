<?php
require_once __DIR__ . '/DAO.php';

class DataDAO extends DAO {

  public function insert_order($data) {
    $sql = "INSERT INTO `store_orders_day` (`store_id`,`average_order`,`total_revenue`,`amount_orders`,`date`) VALUES (:store_id,:average_order,:total_revenue,:amount_orders,:date)";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':store_id', $data['store_id']);
    $stmt->bindValue(':average_order', $data['average_order']);
    $stmt->bindValue(':total_revenue', $data['total_revenue']);
    $stmt->bindValue(':amount_orders', $data['amount_orders']);
    $stmt->bindValue(':date', $data['date']);
    $stmt->execute();
  }

  public function get_stores() {
    $sql = "SELECT * FROM `stores` WHERE `synced_firstTime` = 2";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function check_order($data) {
    $sql = "SELECT * FROM `store_orders_day`
            WHERE `store_id` = :store_id
            AND `date` = :date_from";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':date_from', strip_tags($data['date_from']));
    $stmt->bindValue(':store_id', strip_tags($data['store_id']));
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function get_orders($data) {
    $sql = "SELECT * FROM `store_orders`
            WHERE `store_id` = :store_id
            AND `date` BETWEEN :date_from AND :date_to";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':date_from', strip_tags($data['date_from']));
    $stmt->bindValue(':date_to', strip_tags($data['date_to']));
    $stmt->bindValue(':store_id', strip_tags($data['store_id']));
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }






}
