<?php

date_default_timezone_set('Europe/Amsterdam');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
print_r('<pre>');

$error_log = '/home/admin/domains/indiegroup.be/public_html/cronjobs/count_orders_day/errors.log';
// $error_log = 'errors.log';

error_log("\n\nStart script datum:".date('Y-m-d H:i:s')."\n", 3, $error_log);

require __DIR__ . '/dao/DataDAO.php';
$dataDAO = new DataDAO();

$stores = $dataDAO->get_stores();

foreach($stores as $store){
  error_log("\n\nStore id: ".$store['id']." datum:".date('Y-m-d H:i:s')."\n", 3, $error_log);

  $date_from = date('Y-m-d 00:00:00', strtotime('-1 day', strtotime(date('Y-m-d H:i:s'))));
  $date_to = date('Y-m-d 23:59:59', strtotime('-1 day', strtotime(date('Y-m-d H:i:s'))));

  try{
    $orders = $dataDAO->get_orders(array(
      'date_from' => $date_from,
      'date_to' => $date_to,
      'store_id' => $store['id']
    ));

    $total_price = 0;

    if(!empty($orders)){
      foreach($orders as $order){
        $total_price += (int)$order['base_total_paid'];
      }
      $result = array(
        'average_order' => round($total_price / count($orders), 2),
        'total_revenue' => $total_price,
        'amount_orders' => count($orders)
      );
    }else{
      $result = array(
        'average_order' => 0,
        'total_revenue' => 0,
        'amount_orders' => 0
      );
    }

    $dataDAO->insert_order(array(
      'store_id' => $store['id'],
      'average_order' => $result['average_order'],
      'total_revenue' => $result['total_revenue'],
      'amount_orders' => $result['amount_orders'],
      'date' => $date_from
    ));
  } catch (Exception $e) {
      error_log("\n\n".$e->getMessage(), 3, $error_log);
     break;
  }




}

error_log("\n\End script datum:".date('Y-m-d H:i:s')."\n", 3, $error_log);
