<?php
require_once __DIR__ . '/DAO.php';

class UserDAO extends DAO {

  public function check_email($email) {
    $sql = "SELECT `id`,`username`,`password`, `role_id` FROM `users` WHERE `email` = :email";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':email', $email);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function register($data) {
    $sql = "INSERT INTO `users` (`username`, `email` , `password`, `date`) VALUES (:username, :email, :password, :date)";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':username', $data['username']);
    $stmt->bindValue(':email', $data['email']);
    $stmt->bindValue(':password', $data['password']);
    $stmt->bindValue(':date', $data['date']);
    $stmt->execute();
  }


}
