<?php
require_once __DIR__ . '/DAO.php';

class ApplicationsDAO extends DAO {

  public function get_applications() {
      $sql = "SELECT `applications`.`name`,GROUP_CONCAT(`data_options`.`name`) as `data_option_names` FROM `applications`
              LEFT JOIN `data_options`
              ON `applications`.`id` = `data_options`.`application_id`
              GROUP BY `applications`.`name`";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function create_credential($data) {
    $sql = "INSERT INTO `application_credentials` (`application_id`,`client_id`, `username`, `password`, `token`, `date`)
            VALUES (:application_id, :client_id, :username, :password, :token, :date)";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':application_id', $data['application_id']);
    $stmt->bindValue(':client_id', $data['client_id']);
    $stmt->bindValue(':username', $data['username']);
    $stmt->bindValue(':password', $data['password']);
    $stmt->bindValue(':token', $data['token']);
    $stmt->bindValue(':date', date('Y-m-d H:i:s'));
    $stmt->execute();
  }

  public function update_credential($data) {
    $sql = "UPDATE `application_credentials` SET `date` = `date`";

    if(isset($data['username'])){
      $sql .= " ,`username` = :username";
    }

    if(isset($data['password'])){
      $sql .= " ,`password` = :password";
    }

    if(isset($data['token'])){
      $sql .= " ,`token` = :token";
    }

    $sql .= " WHERE `application_id` = :application_id AND `client_id` = :client_id";
    $stmt = $this->pdo->prepare($sql);

    if(isset($data['username'])){
      $stmt->bindValue(':username', strip_tags($data['username']));
    }

    if(isset($data['password'])){
      $stmt->bindValue(':password', strip_tags($data['password']));
    }

    if(isset($data['token'])){
      $stmt->bindValue(':token', strip_tags($data['token']));
    }

    $stmt->bindValue(':application_id', strip_tags($data['application_id']));
    $stmt->bindValue(':client_id', strip_tags($data['client_id']));
    $stmt->execute();
  }


}
