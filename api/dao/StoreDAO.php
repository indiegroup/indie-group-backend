<?php
require_once __DIR__ . '/DAO.php';

class StoreDAO extends DAO {

  public function get_stores($data) {
    $options = array('id','store_name','temp_sync','store_type','synced_firstTime');
    $sql = "SELECT `id`,`url`,`store_name` as `name`, `url`, `store_type`, `temp_sync`, `synced_firstTime`, `client_id` FROM `stores` WHERE 1";

    foreach($options as $key => $option){
      if(isset($data['params'][$option])){
        $sql .= " AND `".$option."` = :".$option;
      }
    }

    $sql .= " ORDER BY :order_field ".$data['order_by']." LIMIT :page, :limit";
    $stmt = $this->pdo->prepare($sql);

    foreach($options as $key => $option){
      if(isset($data['params'][$option])){
        $stmt->bindValue(':'.$option, $data['params'][$option]);
      }
    }

    $stmt->bindValue(':limit', $data['limit']);
    $stmt->bindValue(':page', $data['page']);
    $stmt->bindValue(':order_field', $data['order_field']);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function create_store($data) {
    $count1 = 0;
    $count2 = 0;
    $options = array('client_id','store_name','url','username','password','store_type','date');
    $sql = "INSERT INTO `stores` (";

            foreach($data as $key => $value){
                if($count1 == 0){
                  $sql .= " `".$key."`";
                }else{
                  $sql .= " ,`".$key."`";
                }
                $count1++;
            }

            $sql .= " ) VALUES (";

              foreach($data as $key => $value){
                  if($count2 == 0){
                    $sql .= " :".$key;
                  }else{
                    $sql .= " , :".$key;
                  }
                  $count2++;
              }

            $sql .= " )";

    $stmt = $this->pdo->prepare($sql);
    foreach($data as $key => $value){
      $stmt->bindValue(':'.$key, $value);
    }
    $stmt->execute();
    $sql = "SELECT * FROM `stores` WHERE `client_id` = :client_id ORDER BY `date` DESC";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':client_id', $data['client_id']);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function update_store($data) {
    $options = array('client_id','store_id','store_name','url','username','password','store_type');
    $sql = "UPDATE `stores` SET `date` = `date`";

    foreach($options as $option){
      if(isset($data[$option])){
        $sql .= " ,`".$option."` = :".$option;
      }
    }

    $sql .= " WHERE `id` = :id";
    $stmt = $this->pdo->prepare($sql);

    foreach($options as $option){
      if(isset($data[$option])){
        $stmt->bindValue(':'.$option, $data[$option]);
      }
    }

    $stmt->bindValue(':id', $data['id']);
    $stmt->execute();
  }

  public function get_store_by_id($data){
    $sql = "SELECT `url`, `username`, `password`, `store_type` FROM `stores` WHERE `id` = :id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $data['store_id']);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function delete_store($store_id){
    $sql = "DELETE FROM `stores` WHERE `id` = :id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $store_id);
    $stmt->execute();
  }

}
