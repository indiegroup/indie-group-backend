<?php
require_once __DIR__ . '/DAO.php';

class DataDAO extends DAO {

  public function get_token() {
    $sql = "SELECT * FROM `teamleader_token`";
    $stmt = $this->pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function update_token($token) {
    $sql = "UPDATE `teamleader_token` SET `token` = :token WHERE `id` = 1";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':token', $token);
    $stmt->execute();
  }


}
