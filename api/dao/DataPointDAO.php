<?php
require_once __DIR__ . '/DAO.php';

class DatapointDAO extends DAO {

  public function create_dataPoint($data) {
    $sql = "INSERT INTO `data_points` (`user_id`,`created_at`) VALUES (:user_id,:created_at)";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':user_id', $data['user_id']);
    $stmt->bindValue(':created_at', $data['created_at']);
    $stmt->execute();
    $sql = "SELECT `id` FROM `data_points` WHERE `user_id` = :user_id ORDER BY `created_at` DESC";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':user_id', $data['user_id']);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function get_dp($data) {
    $sql = "SELECT * FROM `data_points` WHERE 1";

    if(isset($data['params']['user_id'])){
      $sql .= " AND `user_id` = :user_id";
    }

    if(isset($data['params']['client_id'])){
      $sql .= " AND `client_id` = :client_id";
    }

    if(isset($data['params']['project_id'])){
      $sql .= " AND `project_id` = :project_id";
    }

    if(isset($data['params']['name'])){
      $sql .= " AND `name` LIKE :name";
    }

    if(isset($data['params']['tags'])){
      $sql .= " AND `tags` LIKE :tags";
    }

    if(isset($data['params']['category'])){
      $sql .= " AND `category` LIKE :category";
    }

    if(isset($data['params']['public'])){
      $sql .= " AND `public` = :public";
    }

    $sql .= " ORDER BY :order_field ".$data['order_by']." LIMIT :page, :limit";
    $stmt = $this->pdo->prepare($sql);

    if(isset($data['params']['user_id'])){
      if($data['params']['user_id'] == 'current_user'){
        $stmt->bindValue(':user_id', $data['user']['user_id']);
      }else{
        $stmt->bindValue(':user_id', $data['params']['user_id']);
      }
    }

    if(isset($data['params']['client_id'])){
      $stmt->bindValue(':client_id', $data['params']['client_id']);
    }

    if(isset($data['params']['project_id'])){
      $stmt->bindValue(':project_id', $data['params']['project_id']);
    }

    if(isset($data['params']['name'])){
      $stmt->bindValue(':name','%'.$data['params']['name'].'%');
    }

    if(isset($data['params']['tags'])){
      $stmt->bindValue(':tags','%'.$data['params']['tags'].'%');
    }

    if(isset($data['params']['category'])){
      $stmt->bindValue(':category','%'.$data['params']['category'].'%');
    }

    if(isset($data['params']['public'])){
      if($data['params']['public'] == 'true'){
        $stmt->bindValue(':public',1);
      }
      if($data['params']['public'] == 'false'){
        $stmt->bindValue(':public',0);
      }
    }

    $stmt->bindValue(':limit', $data['limit']);
    $stmt->bindValue(':page', $data['page']);
    $stmt->bindValue(':order_field', $data['order_field']);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }


  public function update_dp($data) {
    $sql = "UPDATE `data_points` SET `created_at` = `created_at`";

    if(isset($data['name'])){
      $sql .= " ,`name` = :name";
    }

    if(isset($data['client_id'])){
      $sql .= " ,`client_id` = :client_id";
    }

    if(isset($data['project_id'])){
      $sql .= " ,`project_id` = :project_id";
    }

    if(isset($data['tags'])){
      $sql .= " ,`tags` = :tags";
    }

    if(isset($data['category'])){
      $sql .= " ,`category` = :category";
    }

    if(isset($data['description'])){
      $sql .= " ,`description` = :description";
    }

    if(isset($data['importance'])){
      $sql .= " ,`importance` = :importance";
    }

    if(isset($data['public'])){
      $sql .= " ,`public` = :public";
    }

    if(isset($data['application_id'])){
      $sql .= " ,`application_id` = :application_id";
    }

    if(isset($data['data_option_id'])){
      $sql .= " ,`data_option_id` = :data_option_id";
    }

    if(isset($data['goal'])){
      $sql .= " ,`goal` = :goal";
    }

    if(isset($data['update_freq'])){
      $sql .= " ,`update_freq` = :update_freq";
    }

    $sql .= " WHERE `id` = :id";
    $stmt = $this->pdo->prepare($sql);

    if(isset($data['name'])){
      $stmt->bindValue(':name', strip_tags($data['name']));
    }

    if(isset($data['client_id'])){
      $stmt->bindValue(':client_id', strip_tags($data['client_id']));
    }

    if(isset($data['project_id'])){
      $stmt->bindValue(':project_id', strip_tags($data['project_id']));
    }

    if(isset($data['tags'])){
      $stmt->bindValue(':tags', strip_tags($data['tags']));
    }

    if(isset($data['category'])){
      $stmt->bindValue(':category', strip_tags($data['category']));
    }

    if(isset($data['description'])){
      $stmt->bindValue(':description', strip_tags($data['description']));
    }

    if(isset($data['importance'])){
      $stmt->bindValue(':importance', strip_tags($data['importance']));
    }

    if(isset($data['public'])){
      if($data['public'] == 'true'){
        $stmt->bindValue(':public', 1);
      }
      if($data['public'] == 'false'){
        $stmt->bindValue(':public', 0);
      }
    }

    if(isset($data['application_id'])){
      $stmt->bindValue(':application_id', strip_tags($data['application_id']));
    }

    if(isset($data['data_option_id'])){
      $stmt->bindValue(':data_option_id', strip_tags($data['data_option_id']));
    }

    if(isset($data['goal'])){
      $stmt->bindValue(':goal', strip_tags($data['goal']));
    }

    if(isset($data['update_freq'])){
      $stmt->bindValue(':update_freq', strip_tags($data['update_freq']));
    }

    $stmt->bindValue(':id', strip_tags($data['data_point_id']));
    $stmt->execute();
  }

}
