<?php
require_once __DIR__ . '/DAO.php';

class OrdersDAO extends DAO {

  public function get_orders($data) {
    $sql = "SELECT * FROM `store_orders_day`";
    $sql .= " WHERE `store_id` = :store_id";

    if(isset($data['date_from'])){
      $sql .= " AND `date` BETWEEN :date_from AND :date_to";
    }

    $stmt = $this->pdo->prepare($sql);

    if(isset($data['date_from'])){
      $stmt->bindValue(':date_from', strip_tags($data['date_from']));
      $stmt->bindValue(':date_to', strip_tags($data['date_to']));
    }

    $stmt->bindValue(':store_id', strip_tags($data['store_id']));
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

}
