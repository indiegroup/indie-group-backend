<?php
require_once __DIR__ . '/DAO.php';

class ClientsDAO extends DAO {

  public function get_clients($data) {
    $sql = "SELECT * FROM `clients` WHERE 1";

    if(isset($data['params']['id'])){
      $sql .= " AND `id` = :id";
    }

    if(isset($data['params']['name'])){
      $sql .= " AND `name` LIKE :name";
    }

    $sql .= " ORDER BY :order_field ".$data['order_by']." LIMIT :page, :limit";
    $stmt = $this->pdo->prepare($sql);

    if(isset($data['params']['id'])){
      $stmt->bindValue(':id', $data['params']['id']);
    }

    if(isset($data['params']['name'])){
      $stmt->bindValue(':name', '%'.$data['params']['name'].'%');
    }

    $stmt->bindValue(':limit', $data['limit']);
    $stmt->bindValue(':page', $data['page']);
    $stmt->bindValue(':order_field', $data['order_field']);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function create_client($data) {
    $count1 = 0;
    $count2 = 0;
    $options = array('teamleader_id','name','emails','links','telephones','description','feedback','contact','prioriteit');
    $sql = "INSERT INTO `clients` (";

            foreach($data as $key => $value){
                if($count1 == 0){
                  $sql .= " `".$key."`";
                }else{
                  $sql .= " ,`".$key."`";
                }
                $count1++;
            }

            $sql .= " ) VALUES (";

            foreach($data as $key => $value){
                if($count2 == 0){
                  $sql .= " :".$key;
                }else{
                  $sql .= " , :".$key;
                }
                $count2++;
            }

            $sql .= " )";

    $stmt = $this->pdo->prepare($sql);
    foreach($data as $key => $value){
      $stmt->bindValue(':'.$key, $value);
    }
    $stmt->execute();
    $sql = "SELECT `id` FROM `clients` WHERE `user_id` = :user_id ORDER BY `created_at` DESC";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':user_id', $data['user_id']);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function update_client($data) {
    $options = array('teamleader_id','name','emails','links','telephones','description','feedback','contact','prioriteit');
    $sql = "UPDATE `clients` SET `created_at` = `created_at`";

    foreach($options as $option){
      if(isset($data['params'][$option])){
        $sql .= " ,`".$option."` = :".$option;
      }
    }

    $sql .= " WHERE `id` = :id";
    $stmt = $this->pdo->prepare($sql);

    foreach($options as $option){
      if(isset($data['params'][$option])){
        $stmt->bindValue(':'.$option, $data['params'][$option]);
      }
    }

    $stmt->bindValue(':id', $data['params']['id']);
    $stmt->execute();
  }

  public function get_client_by_id($client_id) {
    $sql = "SELECT * FROM `clients` WHERE `id` = :client_id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':client_id', $client_id);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  public function delete_client($client_id){
    $sql = "DELETE FROM `clients` WHERE `id` = :id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $client_id);
    $stmt->execute();
  }

  public function check_client($teamleader_id) {
    $sql = "SELECT * FROM `clients` WHERE `teamleader_id` = :teamleader_id";
    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':teamleader_id', $teamleader_id);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }


}
