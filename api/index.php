<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH, OPTIONS");

require __DIR__ . '/vendor/autoload.php';


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;


$app = AppFactory::create();
$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();
// $app->setBasePath('/api');
$errorMiddleware = $app->addErrorMiddleware(true, true, true);


$app->get('/', function (Request $request, Response $response) {
    $response->getBody()->write(json_encode("Hello, world!"));
    return $response;
});

/* --- login --- */


$app->post('/login', function (Request $request, Response $response, $args) {
  require './controllers/login.php';

  $results = login($request->getParsedBody());

  response($response, $results);
  return $response;

});

$app->get('/jwtLogin', function (Request $request, Response $response, $args) {
  require './controllers/login.php';
  $jwt = $request->getHeader('Authorization');
  $results = jwt_login($jwt[0]);
  response($response, $results);

  return $response;
});

$app->post('/register', function (Request $request, Response $response, $args) {
  require './controllers/login.php';

  $results = register($request->getParsedBody());

  response($response, $results);
  return $response;

});

/* --- login --- */

/* --- applications --- */

$app->get('/applications', function (Request $request, Response $response) {
  require './controllers/applications.php';

  $results = get_applications();

  response($response, $results);
  return $response;
});

/* --- applications --- */

/* --- clients --- */

$app->get('/clients', function (Request $request, Response $response) {
  require './controllers/clients.php';

  $results = get_clients($request->getHeader('Authorization'), $request->getQueryParams());

  response($response, $results);
  return $response;
});

$app->post('/clients', function (Request $request, Response $response) {
  require './controllers/clients.php';

  $results = create_clients($request->getHeader('Authorization'), $request->getParsedBody());

  response($response, $results);
  return $response;
});

$app->put('/clients', function (Request $request, Response $response) {
  require './controllers/clients.php';

  $results = update_client($request->getHeader('Authorization'), $request->getParsedBody());

  response($response, $results);
  return $response;
});

$app->delete('/clients', function (Request $request, Response $response) {
  require './controllers/clients.php';

  $results = delete_client($request->getHeader('Authorization'),$request->getQueryParams());
  response($response, $results);
  return $response;
});

/* --- clients --- */

/* --- client extra --- */

$app->get('/clientTeamleader', function (Request $request, Response $response) {
  require './controllers/clients.php';

  $results = get_client_teamleader($request->getHeader('Authorization'), $request->getQueryParams());

  response($response, $results);
  return $response;
});

/* --- client extra --- */

/* --- dataPoint --- */

$app->get('/dataPoint', function (Request $request, Response $response) {
  require './controllers/datapoint.php';

  $results = get_dataPoints($request->getHeader('Authorization'),$request->getQueryParams());
  response($response, $results);
  return $response;
});

$app->post('/dataPoint', function (Request $request, Response $response) {
  require __DIR__ . '/controllers/datapoint.php';

  $results = create_dataPoint($request->getHeader('Authorization'));
  response($response, $results);
  return $response;
});

$app->put('/dataPoint', function (Request $request, Response $response) {
  require __DIR__ . '/controllers/datapoint.php';

  $results = update_dataPoint($request->getHeader('Authorization'),$request->getParsedBody());
  response($response, $results);
  return $response;
});

/* --- dataPoint --- */

/* --- Credentials ( wordt tijdelijk niet meer gebruikt -> vervangen door de stores API) --- */

$app->post('/applicationCredentials', function (Request $request, Response $response) {
  require './controllers/applications.php';

  $results = create_credentials($request->getHeader('Authorization'),$request->getParsedBody());
  response($response, $results);
  return $response;
});

$app->put('/applicationCredentials', function (Request $request, Response $response) {
  require './controllers/applications.php';

  $results = update_credentials($request->getHeader('Authorization'),$request->getParsedBody());
  response($response, $results);
  return $response;
});

/* --- Credentials --- */

/* --- stores --- */

$app->get('/stores', function (Request $request, Response $response) {
  require './controllers/stores.php';

  $results = get_stores($request->getHeader('Authorization'),$request->getQueryParams());
  response($response, $results);
  return $response;
});

$app->post('/stores', function (Request $request, Response $response) {
  require './controllers/stores.php';

  $results = create_store($request->getHeader('Authorization'),$request->getParsedBody());
  response($response, $results);
  return $response;
});

$app->put('/stores', function (Request $request, Response $response) {
  require './controllers/stores.php';

  $results = update_store($request->getHeader('Authorization'),$request->getParsedBody());
  response($response, $results);
  return $response;
});

$app->get('/storeOverview', function (Request $request, Response $response) {
  require './controllers/stores.php';

  $results = get_store_overview($request->getHeader('Authorization'),$request->getQueryParams());
  response($response, $results);
  return $response;
});

$app->delete('/stores', function (Request $request, Response $response) {
  require './controllers/stores.php';

  $results = delete_store($request->getHeader('Authorization'),$request->getQueryParams());
  response($response, $results);
  return $response;
});

/* --- stores --- */

/* --- test credentials --- */

$app->post('/testStore', function (Request $request, Response $response) {
  require './controllers/stores.php';

  $results = test_store($request->getHeader('Authorization'),$request->getParsedBody());
  response($response, $results);
  return $response;
});

/* --- test credentials --- */

/* --- orders --- */

$app->get('/orders', function (Request $request, Response $response) {
  require './controllers/order.php';

  $results = get_orders($request->getHeader('Authorization'),$request->getQueryParams());
  response($response, $results);
  return $response;
});

/* --- orders --- */


function response($response, $results){
  if(isset($results['error_message'])){
    $response->getBody()->write(json_encode($results));
    $response->withHeader('Content-Type', 'application/json')->withStatus($results['error_code']);
  }else{
    $response->withHeader('Content-Type', 'application/json');
    $response->getBody()->write(json_encode($results));
  }
}


$app->run();
