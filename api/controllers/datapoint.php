<?php

date_default_timezone_set('Europe/Amsterdam');

require __DIR__ . '/../dao/DataPointDAO.php';
require __DIR__ . '/login.php';


function create_dataPoint($jwt){
  $datapointDAO = new DataPointDAO();

  $user = jwt_login($jwt[0]);
  if($user['role_id'] == 1){
    $response = $datapointDAO->create_dataPoint(array(
      'user_id' => $user['user_id'],
      'created_at' => date('Y-m-d H:i:s')
    ));
    return $response;

  }else{
    return array(
      'error_message' => 'Je hebt niet de juiste rechten om een data point aan te maken',
      'error_code' => 401
    );
  }
}


function get_dataPoints($jwt, $data){
  $datapointDAO = new DataPointDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  if(empty($data['limit'])){
    $limit = 2;
  }else{
    $limit = (int)$data['limit'];
  }

  if(empty($data['page'])){
    $page = 0;
  }else{
    $page = $data['page'];
  }

  if(empty($data['order_field'])){
    $order_field = 'id';
  }else{
    $order_field = $data['order_field'];
  }

  if(empty($data['order_by'])){
    $order_by = 'DESC';
  }else{
    if($data['order_by'] == 'desc'){
      $order_by = 'DESC';
    }
    if($data['order_by'] == 'asc'){
      $order_by = 'ASC';
    }
  }

  try{
    $response = $datapointDAO->get_dp(array(
      'params' => $data,
      'user' => $user,
      'limit' => $limit,
      'page' => $page,
      'order_field' => $order_field,
      'order_by' => $order_by
    ));
  }catch (Exception $e) {
    return array(
      'error_message' => 'Er is iets fout gegaan',
      'error_code' => 400
    );
  }

  return $response;

}


function update_dataPoint($jwt, $params){
  $datapointDAO = new DataPointDAO();

  $user = jwt_login($jwt[0]);
  if($user['role_id'] == 1){
    if(empty($params['id'])){
      return array(
        'error_message' => 'Je moet een id meegeven',
        'error_code' => 401
      );
    }
    try{
      $datapointDAO->update_dp($params);
    }catch (Exception $e) {
      return array(
        'error_message' => 'Er is iets fout gegaan tijdens het updaten',
        'error_code' => 400
      );
    }

    return 'OK';

  }else{
    return array(
      'error_message' => 'Je hebt niet de juiste rechten om een data point aan te maken',
      'error_code' => 401
    );
  }

}

 ?>
