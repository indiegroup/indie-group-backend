<?php

function magento1($store, $new_date_from, $new_date_to){

  try {

    $password = encrypt_decrypt('decrypt',$store['password']);
    $apisoap_v2_url = $store['url']."/api/v2_soap/?wsdl";
    $client = new SoapClient($apisoap_v2_url);
    $session_id = $client->login($store['username'], $password);

  } catch (Exception $e) {
    return array('status' => false,'message' => $e->getMessage());
  }

  $filter = array(
    'filter' => array(array('key' => 'status','value' => 'complete')),
    'complex_filter' => array(
      array(
            'key' => 'UPDATED_AT',
            'value' => array(
                'key' => 'from',
                'value' => $new_date_from
            ),
        ),
        array(
            'key' => 'updated_at',
            'value' => array(
                'key' => 'to',
                'value' => $new_date_to
            ),
        ),
  ));

  try {
    $orders = $client->salesOrderList($session_id, $filter);
  } catch (Exception $e) {
    return array('status' => false,'message' => $e->getMessage());
  }

  return array('status' => true);
}
