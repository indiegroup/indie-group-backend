<?php

function magento2($store, $new_date_from, $new_date_to){


  $password = encrypt_decrypt('decrypt',$store['password']);
  $params = array(
    'username' => $store['username'],
    'password' => $password
  );

  $ch = curl_init();
  

  try {
    curl_setopt($ch, CURLOPT_URL, $store['url'].'/index.php/rest/V1/integration/admin/token?'.urldecode(http_build_query($params)).'');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
  } catch (Exception $e) {
    return array('status' => false,'message' => $e->getMessage());
  }

  $response = curl_exec($ch);
  $data = json_decode($response, true);
  if(isset($data['message'])){
    if($data['message'] == 'De account login was onjuist of uw account is tijdelijk uitgeschakeld. Probeer het later nog eens.'){
      return array('status' => false,'message' => $data['message']);
    }
  }
  if($data == NULL){
    return array('status' => false,'message' => 'Host not found');
  }

  $params = array(
    'searchCriteria[filter_groups][0][filters][0][field]' => 'updated_at', // updated_at of created_at
    'searchCriteria[filter_groups][0][filters][0][value]' => $new_date_from,
    'searchCriteria[filter_groups][0][filters][0][condition_type]' => 'from',
    'searchCriteria[filter_groups][1][filters][1][field]' => 'updated_at',
    'searchCriteria[filter_groups][1][filters][1][value]' => $new_date_to,
    'searchCriteria[filter_groups][1][filters][1][condition_type]' => 'to',
    'searchCriteria[filter_groups][2][filters][2][field]' => 'status',
    'searchCriteria[filter_groups][2][filters][2][value]' => 'complete',
    'searchCriteria[filter_groups][2][filters][2][condition_type]' => 'eq',
    'fields' => 'items[updated_at,base_total_paid,status,store_id,entity_id]'
  );

  $ch = curl_init();

  try {
    curl_setopt($ch, CURLOPT_URL, $store['url'].'/index.php/rest/V1/orders?'.urldecode(http_build_query($params)).'');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BEARER);
    curl_setopt($ch,CURLOPT_XOAUTH2_BEARER,$data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  } catch (Exception $e) {
    return array('status' => false,'message' => $e->getMessage());
  }

  return array('status' => true);
}
