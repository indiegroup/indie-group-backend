<?php

date_default_timezone_set('Europe/Amsterdam');

require __DIR__ . '/../dao/ClientsDAO.php';
require __DIR__ . '/../dao/OrdersDAO.php';
require __DIR__ . '/../dao/StoreDAO.php';
require __DIR__ . '/login.php';
require __DIR__ . '/applications/teamleader.php';

function get_clients($jwt, $params){
  $clientDAO = new ClientsDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  if(empty($params['limit'])){
    $limit = 2;
  }else{
    $limit = (int)$params['limit'];
  }

  if(empty($params['page'])){
    $page = 0;
  }else{
    $page = $params['page'];
  }

  if(empty($params['order_field'])){
    $order_field = 'id';
  }else{
    $order_field = $params['order_field'];
  }

  if(empty($params['order_by'])){
    $order_by = 'DESC';
  }else{
    if($params['order_by'] == 'desc'){
      $order_by = 'DESC';
    }
    if($params['order_by'] == 'asc'){
      $order_by = 'ASC';
    }
  }

  $clients = $clientDAO->get_clients(array(
    'params' => $params,
    'user' => $user,
    'limit' => $limit,
    'page' => $page,
    'order_field' => $order_field,
    'order_by' => $order_by
  ));

  return $clients;
}

function create_clients($jwt, $params){
  $clientDAO = new ClientsDAO();

  $user = jwt_login($jwt[0]);
  if($user['role_id'] != 1){
    return array(
      'error_message' => 'Je hebt niet de juiste rechten om een client aan te maken',
      'error_code' => 401
    );
  }

  $check_client = $clientDAO->check_client($params['teamleader_id']);

  if(empty($check_client)){
    $params['created_at'] = date('Y-m-d H:i:s');
    $params['user_id'] = $user['user_id'];
    $response = $clientDAO->create_client($params);
    return 'OK';
  }else{
    return array(
      'error_message' => 'Client already excists',
      'error_code' => 400
    );
  }


}

function update_client($jwt, $params){
  $clientDAO = new ClientsDAO();


  $user = jwt_login($jwt[0]);
  if($user['role_id'] == 1){
    if(empty($params['id'])){
      return array(
        'error_message' => 'Je moet een id meegeven',
        'error_code' => 400
      );
    }
    $clientDAO->update_client(array(
      'params' => $params
    ));
    return 'OK';

  }else{
    return array(
      'error_message' => 'Je hebt niet de juiste rechten om een client aan te maken',
      'error_code' => 401
    );
  }

}

function get_client_teamleader($jwt, $params){
  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  $results = get_teamleader_client_by_name($params['name']);

  $new_result = [];
  foreach($results['data'] as $key => $result){
    $new_result[$key] = array(
      'name' => $result['name'],
      'teamleader_id' => $result['id'],
      'website' => $result['website'],
      'telephones' => $result['telephones'],
      'emails' => $result['emails']
    );
  }

  return $results;
}

function delete_client($jwt,$data){
  $clientDAO = new ClientsDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }
  if($user['role_id'] != 1){
    return array(
      'error_message' => 'Je hebt niet de juiste rechten om een data point aan te maken',
      'error_code' => 401
    );
  }

  if(empty($data['client_id'])){
    return array(
      'error_message' => 'No client_id found as parameter',
      'error_code' => 400
    );
  }

  $clientDAO->delete_client($data['client_id']);

  return 'OK';

}
