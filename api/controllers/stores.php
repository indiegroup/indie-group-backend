<?php

date_default_timezone_set('Europe/Amsterdam');

require __DIR__ . '/../dao/ClientsDAO.php';
require __DIR__ . '/../dao/OrdersDAO.php';
require __DIR__ . '/../dao/StoreDAO.php';
require __DIR__ . '/login.php';
require __DIR__ . '/encryption.php';
require __DIR__ . '/applications/magento1.php';
require __DIR__ . '/applications/magento2.php';

function get_stores($jwt, $params){
  $storeDAO = new StoreDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  if(empty($params['limit'])){
    $limit = 2;
  }else{
    $limit = (int)$params['limit'];
  }

  if(empty($params['page'])){
    $page = 0;
  }else{
    $page = $params['page'];
  }

  if(empty($params['order_field'])){
    $order_field = 'id';
  }else{
    $order_field = $params['order_field'];
  }

  if(empty($params['order_by'])){
    $order_by = 'DESC';
  }else{
    if($params['order_by'] == 'desc'){
      $order_by = 'DESC';
    }
    if($params['order_by'] == 'asc'){
      $order_by = 'ASC';
    }
  }

  $stores = $storeDAO->get_stores(array(
    'params' => $params,
    'limit' => $limit,
    'page' => $page,
    'order_field' => $order_field,
    'order_by' => $order_by
  ));

  return $stores;
}

function create_store($jwt, $data){
  $storeDAO = new StoreDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  $data['date'] = date('Y-m-d H:i:s');

  if(isset($data['client_id'])){
    if(isset($data['password'])){
      $data['password'] = encrypt_decrypt('encrypt',$data['password']);
    }
    $check = check_if_credentails_correct($data);
    if($check['status'] == false){
      return $check;
    }else{
      $store = $storeDAO->create_store($data);
      return array('store_id' => $store['id']);
    }
  }else{
    return array(
      'error_message' => 'Je moet een client_id meegeven',
      'error_code' => 400
    );
  }



}

function update_store($jwt, $data){
  $storeDAO = new StoreDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  if(isset($data['id'])){
    if(isset($data['password'])){
      $data['password'] = encrypt_decrypt('encrypt',$data['password']);
    }
    $storeDAO->update_store($data);
  }else{
    return array(
      'error_message' => 'Er werd geen id meegegeven',
      'error_code' => 400
    );
  }

  return 'OK';
}

function test_store($jwt, $data){
  $storeDAO = new StoreDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  $store_info = $storeDAO->get_store_by_id($data);
  $check = check_if_credentails_correct($store_info);

  return $check;
}

function check_if_credentails_correct($store){

  $date_from = date('Y-m-d 00:00:00');
  $date_to = date('Y-m-d 01:00:00');

  if($store['store_type'] == 'magento1'){
    $check = magento1($store, $date_from, $date_to);
  }
  if($store['store_type'] == 'magento2'){
    $check = magento2($store, $date_from, $date_to);
  }

  return $check;


}

function get_store_overview($jwt, $params){
  $clientDAO = new ClientsDAO();
  $storeDAO = new StoreDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  if(empty($params['limit'])){
    $limit = 10;
  }else{
    $limit = (int)$params['limit'];
  }

  if(empty($params['page'])){
    $page = 0;
  }else{
    $page = $params['page'];
  }

  if(empty($params['order_field'])){
    $order_field = 'id';
  }else{
    $order_field = $params['order_field'];
  }

  if(empty($params['order_by'])){
    $order_by = 'DESC';
  }else{
    if($params['order_by'] == 'desc'){
      $order_by = 'DESC';
    }
    if($params['order_by'] == 'asc'){
      $order_by = 'ASC';
    }
  }

  $stores = $storeDAO->get_stores(array(
    'user' => $user,
    'limit' => $limit,
    'page' => $page,
    'order_field' => $order_field,
    'order_by' => $order_by
  ));

  foreach($stores as $key => $store){
    $client = $clientDAO->get_client_by_id($store['client_id']);
    unset($client['id']);

    $get_orders = array(
      'current' => array('date_to' => 1, 'date_from' => 8, 'name' => 'current', 'store_id' => $store['id']),
      'previous' => array('date_to' => 8, 'date_from' => 15, 'name' => 'previous', 'store_id' => $store['id']),
    );

    foreach($get_orders as $get_order){
      $orders = get_orders($get_order);
      $stores[$key]['magento_data'][$get_order['name']] = $orders;
    }

    $stores[$key]['type'] = $stores[$key]['store_type'];
    unset($stores[$key]['store_type']);
    $stores[$key]['client'] = $client;

  }

  return $stores;
}

function get_orders($data){
  $ordersDAO = new OrdersDAO();

  $date_from = date('Y-m-d 00:00:00', strtotime('-'.$data['date_from'].' day', strtotime(date('Y-m-d H:i:s'))));
  $date_to = date('Y-m-d 23:59:59', strtotime('-'.$data['date_to'].' day', strtotime(date('Y-m-d H:i:s'))));

  $orders_days = $ordersDAO->get_orders(array(
    'store_id' => $data['store_id'],
    'date_from' => $date_from,
    'date_to' => $date_to
  ));

  $total_price = 0;
  $average_orders = 0;
  $total_orders = 0;
  foreach($orders_days as $days){
    $total_price += (int)$days['total_revenue'];
    $average_orders += (int)$days['average_order'];
    $total_orders += (int)$days['amount_orders'];
  }
  if($total_orders != 0){
    $average = $total_price / $total_orders;
  }else{
    $average = 0;
  }

  if(!empty($orders_days)){
    return array(
      'from' => $date_from,
      'to' => $date_to,
      'average_order' => $average,
      'total_revenue' => $total_price,
      'amount_orders' => $total_orders
    );
  }else{
    return array(
      'from' => $date_from,
      'to' => $date_to,
      'average_order' => 0,
      'total_revenue' => 0,
      'amount_orders' => 0
    );
  }
}

function delete_store($jwt,$data){
  $storeDAO = new StoreDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }
  if($user['role_id'] != 1){
    return array(
      'error_message' => 'Je hebt niet de juiste rechten om een data point aan te maken',
      'error_code' => 401
    );
  }

  if(empty($data['store_id'])){
    return array(
      'error_message' => 'No store_id found as parameter',
      'error_code' => 400
    );
  }

  $storeDAO->delete_store($data['store_id']);

  return 'OK';

}
 ?>
