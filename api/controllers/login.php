<?php

date_default_timezone_set('Europe/Amsterdam');

require __DIR__ . '/../dao/UserDAO.php';
use \Firebase\JWT\JWT;

 $GLOBALS['key'] = "cP59UHiHjPZYC0loEsk7s+hUmT3QHerAQJMZWC11Qrn2N+ybwwNblDKv+s5qgMQ55tNoQ9IfAkEAxkyffU6ythpg/H0Ixe1I2rd0GbF05biIzO/i77Det3n4YsJVlDckZkcvY3SK2iRIL4c9yY6hlIhs+K9wXTtGWwJBAO9Dskl48mO7woPR9uD22jDpNSwek90OMepTjzSvlhjbfuPN1IdhqvSJTDychRwn1kIJ7LQZgQ8fVz9OCFZ/6qMCQGObqaGwHmUK6xzpUbbacnYrIM6nLSkXgOAwv7XXCojvY614ILTK3iXiLBOxPu5Eu13keUz9sHyD6vkgZzjtxXECQAkp4Xerf5TGfQXGXhxIX52yH+N2LtujCdkQZjXAsGdmB2zNzvrlgRmgBrklMTrMYgm1NPcW+bRLGcwgW2PTvNMMIICXAIBAAKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p/llLCrEeQhta5kaQu/RnvuER4W8oDH3+3iuIYW4VQAzyqFpqsdfmlakzjermlkjMLKMLKQMKSDJFAEMQMSKDFwuzjkDI+17t5t0tyazyZ8JXw+KgXTxldMPEL95+qVhgXvwtihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7UrBQIDAQABAoGAb/MXV46XxCFRxNuB8LyAtmLDgi/xRnTAlMHjSACddwkyKem8//8eZtw9fzxzbWZ/1/doQOuHBGYZU8aDzzj59FZ78dyzNFoF91hbvZKkg+6wGyd/LrGVEB+Xre0JNil0GReM2AHDNZUYRv+HYJPIOrB0CRczLQsgFJ8K6aAD6F0CQQDzbpjYdx10qgK1=";

function login($data){
  $userDAO = new UserDAO();
  if(!empty($data['email']) && !empty($data['password'])) {
    $user = $userDAO->check_email($data['email']);
    if($user){
      if (password_verify(strip_tags($data['password']), $user['password'])) {
        $jwt = JWT::encode(array('user_id' => $user['id'], 'role_id' => $user['role_id'], 'name' => $user['username']), $GLOBALS['key']);
        return array(
          'jwt' => $jwt,
          'name' => $user['username'],
          'role_id' => $user['role_id']
        );
      }else{
        return array(
          'error_message' => 'Password or email is wrong',
          'error_code' => 400
        );
      }
    }else{
      return array(
        'error_message' => 'Password or email is wrong',
        'error_code' => 400
      );
    }
  }else{
    return array(
      'error_message' => 'Password or email is wrong',
      'error_code' => 400
    );
  }
}

function jwt_login($jwt){
  try{
    JWT::$leeway = 60;
    $decoded = JWT::decode($jwt, $GLOBALS['key'], array('HS256'));
    $decoded_array = (array) $decoded;
  }catch (Exception $e) {
    return array(
      'error_message' => 'Jwt token is niet gevonden of verkeerd!',
      'error_code' => 401
    );
  }

  return $decoded_array;
}

function register($data){
  $userDAO = new UserDAO();

  if(empty($data['username'])){
    return array(
      'error_message' => 'No username',
      'error_code' => 400
    );
  }

  if(empty($data['email'])){
    return array(
      'error_message' => 'No email',
      'error_code' => 400
    );
  }

  if(strpos($data['email'], '@') === false) {
    return array(
      'error_message' => 'Email not correct',
      'error_code' => 400
    );
  }

  if(strpos($data['email'], '.') === false) {
    return array(
      'error_message' => 'Email not correct',
      'error_code' => 400
    );
  }

  if(empty($data['password'])){
    return array(
      'error_message' => 'No password',
      'error_code' => 400
    );
  }

  $check_email = $userDAO->check_email($data['email']);

  if(!empty($check_email)){
    return array(
      'error_message' => 'Email already exists',
      'error_code' => 400
    );
  }

  $password = password_hash($data['password'], PASSWORD_BCRYPT);

  $userDAO->register(array(
    'username' => $data['username'],
    'email' => $data['email'],
    'password' => $password,
    'date' => date('Y-m-d H:i:s')
  ));

  return 'OK';

}

 ?>
