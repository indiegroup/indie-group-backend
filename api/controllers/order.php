<?php

date_default_timezone_set('Europe/Amsterdam');

require __DIR__ . '/../dao/OrdersDAO.php';
require __DIR__ . '/login.php';


function get_orders($jwt, $data){
  $ordersDAO = new OrdersDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  $orders = $ordersDAO->get_orders($data);

  $diff = strtotime($data['date_to']) - strtotime($data['date_from']);
  $diff_days = round($diff / (60 * 60 * 24));


  $previous_date_from = date('Y-m-d 00:00:00', strtotime('-'.$diff_days.' day', strtotime($data['date_from'])));
  $previous_date_to = date('Y-m-d 00:00:00', strtotime('-'.$diff_days.' day', strtotime($data['date_to'])));

  $get_orders = array(
    'current' => array('date_to' => $data['date_to'], 'date_from' => $data['date_from'], 'name' => 'current', 'store_id' => $data['store_id']),
    'previous' => array('date_to' => $previous_date_to, 'date_from' => $previous_date_from, 'name' => 'previous', 'store_id' => $data['store_id']),
  );

  foreach($get_orders as $get_order){
    $orders = get_orders_data($get_order);
    $result[$get_order['name']] = $orders;
  }

  return $result ;


}


function get_orders_data($data){
  $ordersDAO = new OrdersDAO();


  $orders_days = $ordersDAO->get_orders(array(
    'store_id' => $data['store_id'],
    'date_from' => $data['date_from'],
    'date_to' => $data['date_to']
  ));

  $total_price = 0;
  $average_orders = 0;
  $total_orders = 0;
  foreach($orders_days as $days){
    $total_price += (int)$days['total_revenue'];
    $average_orders += (int)$days['average_order'];
    $total_orders += (int)$days['amount_orders'];
  }
  if($total_orders != 0){
    $average = $total_price / $total_orders;
  }else{
    $average = 0;
  }

  if(!empty($orders_days)){
    return array(
      'from' => $data['date_from'],
      'to' => $data['date_to'],
      'average_order' => $average,
      'total_revenue' => $total_price,
      'amount_orders' => $total_orders
    );
  }else{
    return array(
      'from' => $data['date_from'],
      'to' => $data['date_to'],
      'average_order' => 0,
      'total_revenue' => 0,
      'amount_orders' => 0
    );
  }
}
