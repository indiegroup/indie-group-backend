<?php

date_default_timezone_set('Europe/Amsterdam');
require __DIR__ . '/../dao/ApplicationsDAO.php';
require __DIR__ . '/login.php';


function get_applications(){
  $applicationsDAO = new ApplicationsDAO();

  $applications = $applicationsDAO->get_applications();

  $new_applications = [];
  foreach($applications as $application){
    $options = explode(',',$application['data_option_names']);
    array_push($new_applications, array(
      'name' => $application['name'],
      'data_option_names' => $options
  ));
  }
  return $new_applications;
}


function create_credentials($jwt,$data){
  $applicationsDAO = new ApplicationsDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }


  if(isset($data['application_id']) && isset($data['client_id'])){
    $applicationsDAO->create_credential($data);
  }else{
    return array(
      'error_message' => 'Niet alle velden zijn correct ingevuld',
      'error_code' => 400
    );
  }

  return 'created';

}

function update_credentials($jwt,$data){
  $applicationsDAO = new ApplicationsDAO();

  $user = jwt_login($jwt[0]);
  if(isset($user['error_code'])){
    return $user;
  }

  $applicationsDAO->update_credential($data);

  return 'updated';

}
